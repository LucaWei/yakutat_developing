#include <cmath>
#include <omp.h>
#include <iostream>

#include "Resolution.h"

using std::cout;
using std::endl;


void SuccessiveOverRelaxation
    (
    // ======================================================== //
    double zeta, int itmax, double dt, double omega,

    double (*pre)[ny][nz],
    double (*p)[ny][nz],
    double (*u_star)[ny][nz],
    double (*v_star)[ny][nz],
    double (*w_star)[ny][nz],

    double (*iDx),
    double (*Dxs),
    double (*iDy),
    double (*Dys),
    double (*iDz),
    double (*Dzs)

    // ======================================================== //
    
    )
{
    // ======================================================== //
    int ik;
    double pChangeMax;
    double mChangeMax;
    double mChange;
    double pNEW;
    double pChange;
    int i, j, k;
    // ======================================================== //

    ik = 0;
    pChangeMax = 1.0;
    mChangeMax = 1.0;

    while(pChangeMax>zeta && ik <itmax)
    {
        
        
        pChangeMax = 0.0;
        mChangeMax = 0.0;


        for(size_t i = gCells; i < nx-gCells; ++i )
        {
            for(size_t j = gCells; j < ny-gCells; ++j )
            {
                for(size_t k = gCells; k < nz-gCells; ++k)
                {
                    mChange = ( u_star[i][j][k] - u_star[i-1][j][k] ) * iDy[j] * iDz[k] \
                            + ( v_star[i][j][k] - v_star[i][j-1][k] ) * iDx[i] * iDz[k] \
                            + ( w_star[i][j][k] - w_star[i][j][k-1] ) * iDx[i] * iDy[j];



                    pNEW = (
                            - p[i+1][j][k] * iDy[j] * iDz[k] / Dxs[i] 
                            - p[i-1][j][k] * iDy[j] * iDz[k] / Dxs[i-1] 
                            - p[i][j+1][k] * iDx[i] * iDz[k] / Dys[j] 
                            - p[i][j-1][k] * iDx[i] * iDz[k] / Dys[j-1] 
                            - p[i][j][k+1] * iDx[i] * iDy[j] / Dzs[k] 
                            - p[i][j][k-1] * iDx[i] * iDy[j] / Dzs[k-1] 
                            + mChange / dt
                            ) 
                            /  
                            (- iDy[j] * iDz[k] / Dxs[i] - iDy[j] * iDz[k] / Dxs[i-1] 
                             - iDx[i] * iDz[k] / Dys[j] - iDx[i] * iDz[k] / Dys[j-1] 
                             - iDx[i] * iDy[j] / Dzs[k] - iDx[i] * iDy[j] / Dzs[k-1] );

                    pChange = std::abs(pNEW - p[i][j][k]);

                    p[i][j][k] += omega * (pNEW - p[i][j][k]);
                    


                    if(pChange > pChangeMax)
                        pChangeMax = pChange;

                    
                    
                }
            }    
        }
        
        ++ik;
        


    }
    
    --ik;

    


    cout << "===================================================================="     << endl;
    cout << "Num Of Iter = " << ik << ", Residual = " << pChangeMax << endl;



    
    





}
