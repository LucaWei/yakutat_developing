#include <stdlib.h> 
#include <stdio.h>
#include <math.h>
#include <omp.h>


#include "Resolution.h"

void InitialConditions
    (
    // ======================================================== //
    //vector<vector<vector<double> > > u( nx+4, vector<vector<double> >(ny+4,vector<double> (nz+4)) ),
    double (*u)[ny][nz],
    double (*v)[ny][nz],
    double (*w)[ny][nz],
    
    double (*u1)[ny][nz],
    double (*v1)[ny][nz],
    double (*w1)[ny][nz],

    double (*u2)[ny][nz],
    double (*v2)[ny][nz],
    double (*w2)[ny][nz],

    double (*u_star)[ny][nz],
    double (*v_star)[ny][nz],
    double (*w_star)[ny][nz],

    double (*ETA)[ny][nz],


    double (*p)[ny][nz]
    // ======================================================== //
    )
{


    // ======================================================== //

    int i, j, k;

    // ======================================================== //

    for(i = 0; i < nx; ++i){
        for(j = 0; j < ny; ++j){
            for(k = 0; k < nz; ++k){
                u[i][j][k] = 0.0;
                v[i][j][k] = 0.0;
                w[i][j][k] = 0.0;
                u1[i][j][k] = 0.0;
                v1[i][j][k] = 0.0;
                w1[i][j][k] = 0.0;
                u2[i][j][k] = 0.0;
                v2[i][j][k] = 0.0;
                w2[i][j][k] = 0.0;
                u_star[i][j][k] = 0.0;
                v_star[i][j][k] = 0.0;
                w_star[i][j][k] = 0.0;
                p[i][j][k] = 0.0;
                ETA[i][j][k] = 0.0;
            }
        }
    }








}