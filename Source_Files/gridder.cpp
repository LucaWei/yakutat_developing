#include <stdlib.h> 
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <cstring>
#include <iostream>


#include "Resolution.h"

using std::cout;
using std::endl;


void gridder
    (
    // ======================================================== //
    double GridderXc, double GridderYc, double GridderZc, 
    double lxSml, double lySml, double lzSml, double dx,
    double dy, double dz, double dxSml, double dySml, double dzSml,
    char *Gridder,


    double (*X),
    double (*Y),
    double (*Z),

    double (*iDx),
    double (*Dxs),
    double (*iDy),
    double (*Dys),
    double (*iDz),
    double (*Dzs),

    double (*Xa),
    double (*Ya),
    double (*Za),

    double (*Xs),
    double (*Ys),
    double (*Zs)
    // ======================================================== //
    )


{
    // ======================================================== //

    const double xSBgn = GridderXc - lxSml/2.0;

    const double xSEnd = GridderXc + lxSml/2.0;

    const double ySBgn = GridderYc - lySml/2.0;

    const double ySEnd = GridderYc + lySml/2.0;

    const double zSBgn = GridderZc - lzSml/2.0;

    const double zSEnd = GridderZc + lzSml/2.0;


    double xNextLrgValue;

    double xNextSmlValue;
    
    double yNextLrgValue;
    
    double yNextSmlValue;
    
    double zNextLrgValue;
    
    double zNextSmlValue;


    int Nblock = 1;
	int tempNX = nx-gCells*2;
	int tempNY = ny-gCells*2;
	int tempNZ = nz-gCells*2;
	int N_total = tempNX*tempNY*tempNZ;


	float (*Xout)[ny-gCells*2][nz-gCells*2] = new float[nx-gCells*2][ny-gCells*2][nz-gCells*2];
	float (*Yout)[ny-gCells*2][nz-gCells*2] = new float[nx-gCells*2][ny-gCells*2][nz-gCells*2];
	float (*Zout)[ny-gCells*2][nz-gCells*2] = new float[nx-gCells*2][ny-gCells*2][nz-gCells*2];

    // ======================================================== //



    if(strcmp(Gridder, "non-uniform")  == 0)
    {

        //Unequal grid intervals
        // ======================================================== //

        for(size_t i = 2; i < nx-1; ++i)
        {
            xNextLrgValue =  X[i-1] + dx;   
            xNextSmlValue =  X[i-1] + dxSml;  

            if(i == 2){

                X[i] = 0.0;

            }
            else if(xNextLrgValue > xSBgn)
            {

                if(xNextSmlValue > xSEnd)
                {

                    X[i] = xNextLrgValue;


                }
                else{

                    X[i] = xNextSmlValue;

                }
            }
            else{
                X[i] = xNextLrgValue;
            }
            

        }

        for(size_t j = 2; j < ny-1; ++j)
        {
            yNextLrgValue =  Y[j-1] + dy;   
            yNextSmlValue =  Y[j-1] + dySml;  

            if(j == 2)
            {

                Y[j] = 0.0;

            }
            else if(yNextLrgValue > ySBgn)
            {

                if(yNextSmlValue > ySEnd)
                {

                    Y[j] = yNextLrgValue;

                }
                else{

                    Y[j] = yNextSmlValue;

                }

                

            }
            else{
                Y[j] = yNextLrgValue;
            }


        }


        for(size_t k = 2; k < nz-1; ++k)
        {
            zNextLrgValue =  Z[k-1] + dz;   
            zNextSmlValue =  Z[k-1] + dzSml;  

            if(k == 2)
            {

                Z[k] = 0.0;

            }
            else if(zNextLrgValue > zSBgn)
            {

                if(zNextSmlValue > zSEnd)
                {

                    Z[k] = zNextLrgValue;

                }
                else{

                    Z[k] = zNextSmlValue;

                }

                

            }
            else
            {
                Z[k] = zNextLrgValue;
            }


        }

        
        // ======================================================== //


    }
    else if(strcmp(Gridder, "uniform")  == 0)
    {

        //equal grid intervals
        // ======================================================== //
        for(size_t i = 2; i < nx-1; ++i)
        {
            if(i==2)
            {
                X[i] = 0.0;
                X[i-1] = X[i] - dx;
                X[i-2] = X[i-1] - dx;
            }
            else{
                X[i] = X[i-1] + dx;
            }
        }


        for(size_t j = 2; j < ny-1; ++j)
        {
            if(j==2)
            {
                Y[j] = 0.0;
                Y[j-1] = Y[j] - dy;
                Y[j-2] = Y[j-1] - dy;
            }
            else
            {
                Y[j] = Y[j-1] + dy;
            }
        }
        
        for(size_t k = 2; k < nz-1; ++k)
        {
            if(k==2)
            {
                Z[k] = 0.0;
                Z[k-1] = Z[k] - dz;
                Z[k-2] = Z[k-1] - dz;
            }
            else
            {
                Z[k] = Z[k-1] + dz;
            }
        }
        // ======================================================== //
    }

    



    




    // Define each of the directional grid lengths
    for (size_t i = 2; i < nx-3; ++i)
    {
        iDx[i] = ( X[i+1] - X[i] );
        Dxs[i] = ( X[i+2] - X[i] ) / 2.0;
    }

    for (size_t j = 2; j < ny-3; ++j)
    {
        iDy[j] = ( Y[j+1] - Y[j] );
        Dys[j] = ( Y[j+2] - Y[j] ) / 2.0;
    }


    for (size_t k = 2; k < nz-3; ++k)
    {
        iDz[k] = ( Z[k+1] - Z[k] );
        Dzs[k] = ( Z[k+2] - Z[k] ) / 2.0;
    }


    // Ghost boundary grid lengths
    iDx[1]    = iDx[2];
    iDx[0]    = iDx[2];
    iDx[nx-3] = X  [nx-2] - X[nx-3];
    iDx[nx-2] = iDx[nx-3];
    iDx[nx-1] = iDx[nx-3];

    Dxs[1]    = Dxs[2];
    Dxs[0]    = Dxs[2];
    Dxs[nx-3] = Dxs[nx-4];
    Dxs[nx-2] = Dxs[nx-4];
    Dxs[nx-1] = Dxs[nx-4];

    iDy[1]    = iDy[2];
    iDy[0]    = iDy[2];
    iDy[ny-3] = Y [ny-2] - Y[ny-3];
    iDy[ny-2] = iDy[ny-3];
    iDy[ny-1] = iDy[ny-3];

    Dys[1]    = Dys[2];
    Dys[0]    = Dys[2];
    Dys[ny-3] = Dys[ny-4];
    Dys[ny-2] = Dys[ny-4];
    Dys[ny-1] = Dys[ny-4];

    iDz [1]    = iDz[2];
    iDz [0]    = iDz[2];
    iDz [nz-3] = Z  [nz-2] - Z[nz-3];
    iDz [nz-2] = iDz[nz-3];
    iDz [nz-1] = iDz[nz-3];

    Dzs[1]    = Dzs[2];
    Dzs[0]    = Dzs[2];
    Dzs[nz-3] = Dzs[nz-4];
    Dzs[nz-2] = Dzs[nz-4];
    Dzs[nz-1] = Dzs[nz-4];



    // Modifying the index of X, Y and Z arrays to represent the actual grid
    for (size_t i = 0; i < nx-3; ++i)
    {
        Xa[i] = X[i+2];
    }

    for (size_t j = 0; j < ny-3; ++j)
    {
        Ya[j] = Y[j+2];
    }

    for (size_t k = 0; k < nz-3; ++k)
    {
        Za[k] = Z[k+2];
    }



    // Defining the midpoint values of the grids
    for (size_t i = 0; i < nx-4; ++i)
    {
        Xs[i] = ( Xa[i] + Xa[i+1] ) / 2.0;
    }

    for (size_t j = 0; j < ny-4; ++j)
    {
        Ys[j] = ( Ya[j] + Ya[j+1] ) / 2.0;
        
    }

    for (size_t k = 0; k < nz-4; ++k)
    {
        Zs[k] = ( Za[k] + Za[k+1] ) / 2.0;

    }

    


    




	for(size_t i = 0; i < nx-gCells*2; ++i )
	{
        for(size_t j = 0; j < ny-gCells*2; ++j )
		{
            for(size_t k = 0; k < nz-gCells*2; ++k)
			{

				Xout[i][j][k] = Xs[i];
				Yout[i][j][k] = Ys[j];
				Zout[i][j][k] = Zs[k];

			}
		}
	}

    

    



	char LESdata[100];
	FILE *fptr;
	sprintf(LESdata,"P3D""%0.5d"".x",0);
	fptr = fopen(LESdata,"wb");

	fwrite(&Nblock, sizeof(int), 1,fptr);


	fwrite(&tempNX, sizeof(int), 1,fptr);
	fwrite(&tempNY, sizeof(int), 1,fptr);
	fwrite(&tempNZ, sizeof(int), 1,fptr);

    for (size_t k = 0; k < nz-gCells*2; ++k) 
	{
		for (size_t j = 0; j < ny-gCells*2; ++j) 
		{ 
			for (size_t i = 0; i < nx-gCells*2; ++i) 
			{

				fwrite(&Xout[i][j][k],sizeof(float),1,fptr);

			}
		}
	}

    for (size_t k = 0; k < nz-gCells*2; ++k) 
	{
		for (size_t j = 0; j < ny-gCells*2; ++j) 
		{ 
			for (size_t i = 0; i < nx-gCells*2; ++i) 
			{

				fwrite(&Yout[i][j][k],sizeof(float),1,fptr);

			}
		}
	}

    for (size_t k = 0; k < nz-gCells*2; ++k) 
	{
		for (size_t j = 0; j < ny-gCells*2; ++j) 
		{ 
			for (size_t i = 0; i < nx-gCells*2; ++i) 
			{

				fwrite(&Zout[i][j][k],sizeof(float),1,fptr);

			}
		}
	}


	
    fclose(fptr);

    delete [] Xout;
	delete [] Yout;
	delete [] Zout;

    
}

    








