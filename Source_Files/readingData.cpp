#include <stdlib.h> 
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <cstring>
#include "Resolution.h"



void readingData
    (
    // ======================================================== //
    double &dx, double &dy, double &dz, 
    const double lx, const double ly, const double lz, 
    const double lxSml, const double lySml, const double lzSml,
    char *Gridder, 
    const int nx, const int ny, const int nz,
    const double nxSml, const double nySml, const double nzSml, 
    double &dxSml, double &dySml, double &dzSml,
    double Re, double &nu
    // ======================================================== //
    )
{
    if(strcmp(Gridder, "non-uniform")  == 0){

        //Small interval
        dxSml = lxSml/nxSml;
        dySml = lySml/nySml;
        dzSml = lzSml/nzSml;


        //Large interval
        dx = ( lx-lxSml ) / ( nx - nxSml ); 
        dy = ( ly-lySml ) / ( ny - nySml );
        dz = ( lz-lzSml ) / ( nz - nzSml );


    }
    else if(strcmp(Gridder, "uniform")  == 0){

        dx = lx / (nx-2.0*gCells); 
        dy = ly / (ny-2.0*gCells);
        dz = lz / (nz-2.0*gCells);

    }

       

    nu = 1./Re;

}