#include <cmath>
#include <omp.h>
#include <cstring>
#include <stdio.h>
#include <iostream>

#include "Resolution.h"

void Output_Q_Cpp_PLOT3D
    (
    // ======================================================== //
    int istep,

    double (*p)[ny][nz],

    double (*u2)[ny][nz],
    double (*v2)[ny][nz],
    double (*w2)[ny][nz],

	double (*ETA)[ny][nz]
    
    // ======================================================== //
    )
{

    // ======================================================== //
    double (*uc)[ny-gCells*2][nz-gCells*2] = new double[nx-gCells*2][ny-gCells*2][nz-gCells*2];
    double (*vc)[ny-gCells*2][nz-gCells*2] = new double[nx-gCells*2][ny-gCells*2][nz-gCells*2];
    double (*wc)[ny-gCells*2][nz-gCells*2] = new double[nx-gCells*2][ny-gCells*2][nz-gCells*2];

	

    int Nblock = 1;
	int tempNX = nx-gCells*2;
	int tempNY = ny-gCells*2;
	int tempNZ = nz-gCells*2;
	int N_total = tempNX*tempNY*tempNZ;

    double temp = 1.0;    // mach, alpha, reyn, time //

    float(*QUout)[ny-gCells*2][nz-gCells*2] = new float[nx-gCells*2][ny-gCells*2][nz-gCells*2];


    // ======================================================== //



    for(size_t i = 0; i < nx-gCells*2; ++i )
	{
        for(size_t j = 0; j < ny-gCells*2; ++j )
		{
            for(size_t k = 0; k < nz-gCells*2; ++k)
			{
				
                uc[i][j][k] = 0.5 * (u2[i+1][j+gCells][k+gCells] + u2[i+gCells][j+gCells][k+gCells]);
                vc[i][j][k] = 0.5 * (v2[i+gCells][j+1][k+gCells] + v2[i+gCells][j+gCells][k+gCells]);
                wc[i][j][k] = 0.5 * (w2[i+gCells][j+gCells][k+1] + w2[i+gCells][j+gCells][k+gCells]);


            }
        }
    }

	static int io;
    char LESdata[100];
	FILE *fptrQ;
	sprintf(LESdata,"P3D""%0.5d"".q",io);
	fptrQ = fopen(LESdata,"wb");
	io ++;

	fwrite(&Nblock, sizeof(int), 1,fptrQ);

	fwrite(&tempNX, sizeof(int), 1,fptrQ);
	fwrite(&tempNY, sizeof(int), 1,fptrQ);
	fwrite(&tempNZ, sizeof(int), 1,fptrQ);

	fwrite(&temp, sizeof(float), 1,fptrQ);
	fwrite(&temp, sizeof(float), 1,fptrQ);
	fwrite(&temp, sizeof(float), 1,fptrQ);
	fwrite(&temp, sizeof(float), 1,fptrQ);




	for(size_t i = 0; i < nx-gCells*2; ++i )
	{
        for(size_t j = 0; j < ny-gCells*2; ++j )
		{
            for(size_t k = 0; k < nz-gCells*2; ++k)
			{

				QUout[i][j][k] = p[i+gCells][j+gCells][k+gCells];

			}
		}
	}

	for (size_t k = 0; k < nz-gCells*2; ++k) 
	{
		for (size_t j = 0; j < ny-gCells*2; ++j) 
		{ 
			for (size_t i = 0; i < nx-gCells*2; ++i) 
			{

				fwrite(&QUout[i][j][k],sizeof(float),1,fptrQ);

			}
		}
	}


	for(size_t i = 0; i < nx-gCells*2; ++i )
	{
        for(size_t j = 0; j < ny-gCells*2; ++j )
		{
            for(size_t k = 0; k < nz-gCells*2; ++k)
			{

				QUout[i][j][k] = uc[i][j][k];

			}
		}
	}

	for (size_t k = 0; k < nz-gCells*2; ++k) 
	{
		for (size_t j = 0; j < ny-gCells*2; ++j) 
		{ 
			for (size_t i = 0; i < nx-gCells*2; ++i) 
			{

				fwrite(&QUout[i][j][k],sizeof(float),1,fptrQ);

			}
		}
	}



	for(size_t i = 0; i < nx-gCells*2; ++i )
	{
        for(size_t j = 0; j < ny-gCells*2; ++j )
		{
            for(size_t k = 0; k < nz-gCells*2; ++k)
			{

				QUout[i][j][k] = vc[i][j][k];

			}
		}
	}

	for (size_t k = 0; k < nz-gCells*2; ++k) 
	{
		for (size_t j = 0; j < ny-gCells*2; ++j) 
		{ 
			for (size_t i = 0; i < nx-gCells*2; ++i) 
			{

				fwrite(&QUout[i][j][k],sizeof(float),1,fptrQ);

			}
		}
	}



	for(size_t i = 0; i < nx-gCells*2; ++i )
	{
        for(size_t j = 0; j < ny-gCells*2; ++j )
		{
            for(size_t k = 0; k < nz-gCells*2; ++k)
			{

				QUout[i][j][k] = wc[i][j][k];

			}
		}
	}

	for (size_t k = 0; k < nz-gCells*2; ++k) 
	{
		for (size_t j = 0; j < ny-gCells*2; ++j) 
		{ 
			for (size_t i = 0; i < nx-gCells*2; ++i) 
			{

				fwrite(&QUout[i][j][k],sizeof(float),1,fptrQ);

			}
		}
	}



    for(size_t i = 0; i < nx-gCells*2; ++i )
	{
        for(size_t j = 0; j < ny-gCells*2; ++j )
		{
            for(size_t k = 0; k < nz-gCells*2; ++k)
			{

				QUout[i][j][k] = ETA[i+gCells][j+gCells][k+gCells];

			}
		}
	}

	for (size_t k = 0; k < nz-gCells*2; ++k) 
	{
		for (size_t j = 0; j < ny-gCells*2; ++j) 
		{ 
			for (size_t i = 0; i < nx-gCells*2; ++i) 
			{

				fwrite(&QUout[i][j][k],sizeof(float),1,fptrQ);

			}
		}
	}

    



	fclose(fptrQ);

	delete [] QUout;


}
