#ifndef _BOUNDARYCONDITIONS_
#define _BOUNDARYCONDITIONS_

void BoundaryConditions
(
    double (*u)[ny][nz],
    double (*v)[ny][nz],
    double (*w)[ny][nz],

    double (*u1)[ny][nz],
    double (*v1)[ny][nz],
    double (*w1)[ny][nz],

    double (*u2)[ny][nz],
    double (*v2)[ny][nz],
    double (*w2)[ny][nz],

    double (*u_star)[ny][nz],
    double (*v_star)[ny][nz],
    double (*w_star)[ny][nz],

    double (*p)[ny][nz]

);

    

#endif