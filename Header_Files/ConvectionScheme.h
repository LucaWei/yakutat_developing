#ifndef _CONVECTIONSCHEME_INCLUDE_
#define _CONVECTIONSCHEME_INCLUDE_



void DiscretisationQUICK
    (
        double dt, double nu,

        double (*u)[ny][nz],
        double (*v)[ny][nz],
        double (*w)[ny][nz],

        double (*u_star)[ny][nz],
        double (*v_star)[ny][nz],
        double (*w_star)[ny][nz],

        double (*iDx),
        double (*Dxs),
        double (*iDy),
        double (*Dys),
        double (*iDz),
        double (*Dzs)

    
    );






#endif