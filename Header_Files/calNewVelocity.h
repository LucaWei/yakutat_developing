#ifndef _CALNEWVELOCITY_INCLUDE_
#define _CALNEWVELOCITY_INCLUDE_



void calNewVelocity
    (
        double dt,

        double (*p)[ny][nz],
        double (*u_star)[ny][nz],
        double (*v_star)[ny][nz],
        double (*w_star)[ny][nz],
        double (*u1)[ny][nz],
        double (*v1)[ny][nz],
        double (*w1)[ny][nz],
        double (*u2)[ny][nz],
        double (*v2)[ny][nz],
        double (*w2)[ny][nz],
        double (*ETA)[ny][nz],
        double (*FX)[ny][nz],
        double (*FY)[ny][nz],
        double (*FZ)[ny][nz],

        double (*iDx),
        double (*Dxs),
        double (*iDy),
        double (*Dys),
        double (*iDz),
        double (*Dzs)

    
    );

void updating
    (
 


        double (*u)[ny][nz],
        double (*v)[ny][nz],
        double (*w)[ny][nz],
        double (*u2)[ny][nz],
        double (*v2)[ny][nz],
        double (*w2)[ny][nz]




    );




#endif
