
#ifndef _GRIDDER_INCLUDED_
#define _GRIDDER_INCLUDED_

void gridder(double GridderXc, double GridderYc, double GridderZc, 
            double lxSml, double lySml, double lzSml, double dx,
            double dy, double dz, double dxSml, double dySml, double dzSml,
            char *Gridder,


            double (*X),
            double (*Y),
            double (*Z),

            double (*iDx),
            double (*Dxs),
            double (*iDy),
            double (*Dys),
            double (*iDz),
            double (*Dzs),

            double (*Xa),
            double (*Ya),
            double (*Za),

            double (*Xs),
            double (*Ys),
            double (*Zs)
             );


#endif

