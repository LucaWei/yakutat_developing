#ifndef _RESOLUTiON_
#define _RESOLUTiON_

    const int gCells = 2;

    const int    nx  = 40 + gCells*2;
          
    const int    ny  = 60 + gCells*2;
          
    const int    nz  = 80 + gCells*2;

    const double lx  = 1;
          
    const double ly  = 1;
          
    const double lz  = 1;

    

#endif